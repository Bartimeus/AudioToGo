<?php

namespace Drupal\soundtact_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\soundtact_api\Api\ForbiddenJsonException;
use Drupal\soundtact_api\Api\JsonException;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Logic for the Route api.
 */
class UserController extends ControllerBase {
  /**
   * This is the role of the user class, this user has no admin rights.
   */
  const CLIENT_ROLE = 'user';

  /**
   * This is the role of the administrator, this user has admin rights.
   */
  const ADMINISTRATOR_ROLE = 'beheerder';

  /**
   * The request from the user.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Entity type manager object.
   */
  protected $entityTypeManager;

  /**
   * RouteController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $currentRequest
   *   The current request.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(Request $currentRequest, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentRequest = $currentRequest;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get a list of all the users.
   */
  public function getUsers() {
    if (!$this->currentUser()->hasPermission('access user profiles') ||
      !$this->currentUser()->hasPermission('create users')) {
      return new ForbiddenJsonException($this->t("You don't have access to get the users."));
    }

    $user_storage = $this->entityTypeManager()->getStorage('user');

    $current_user = $user_storage->load($this->currentUser->id());
    $user_theme = $current_user->field_theme ? $current_user->field_theme->getValue() : NULL;

    if (empty($user_theme)) {
      return new JsonException($this->t('You seem to have no theme, please contact a administrator'));
    }

    $themes = [];
    foreach ($user_theme as $theme) {
      $themes[] = $theme['target_id'];
    }

    $query = $user_storage->getQuery()
      ->condition('status', 1)
      ->condition('field_theme', $themes, 'IN')
      ->condition('roles', UserController::CLIENT_ROLE);

    $user_ids = $query->execute();
    $users = $user_storage->loadMultiple($user_ids);

    $return_users = [];
    foreach ($users as $user) {
      $name = $user->field_firstname->getValue()[0]['value'] . ' ' . $user->field_lastname->getValue()[0]['value'];

      $return_users[] = [
        'id' => (int) $user->id(),
        'name' => $name,
      ];
    }

    $response = new JsonResponse(['users' => $return_users], 200);
    return $response;
  }

  /**
   * Get all routes by the given user.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user which the routes need to be retrieved from.
   */
  public function getUserRoutes(User $user) {
    if (!$this->currentUser()->hasPermission('access user profiles') ||
      !$this->currentUser()->hasPermission('create users')) {
      return new ForbiddenJsonException($this->t("You don't have access to get the users."));
    }

    $user_theme = $user->field_theme ? $user->field_theme->getValue()[0]['target_id'] : NULL;

    if (empty($user_theme)) {
      return new JsonException($this->t('The user seem to have no theme, please contact a administrator'));
    }

    $route_storage = $this->entityTypeManager()->getStorage('node');
    $query = $route_storage->getQuery()
      ->condition('type', 'route')
      ->condition('field_clients', $user->id(), 'CONTAINS');

    $route_ids = $query->execute();
    $routes = $route_storage->loadMultiple($route_ids);

    $return_routes = [];
    foreach ($routes as $route) {
      $route_name = $route->title ? $route->title->getValue()[0]['value'] : $this->t('No name found for this route.');

      $return_routes[] = [
        'id' => (int) $route->id(),
        'name' => $route_name,
      ];
    }

    $response = new JsonResponse(['routes' => $return_routes], 200);
    return $response;
  }

  /**
   * Get detail information about the current logged in user.
   */
  public function getCurrentUserDetail() {
    $return_array = [];
    $current_user_id = (int) $this->currentUser()->id();
    $current_user = $this->entityTypeManager->getStorage('user')->load($current_user_id);
    if ($current_user !== NULL) {
      $return_array['roles'] = $current_user->getRoles();
      // The first user registered has no roles, but should have all the access.
      if ($current_user_id === 1) {
        $return_array['roles'] = ['administrator'];
      }
    }
    $response = new JsonResponse($return_array);
    return $response;
  }

}
