<?php

namespace Drupal\soundtact_api\Normalizer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\serialization\Normalizer\ContentEntityNormalizer as SerializationContentEntityNormalizer;
use Drupal\node\NodeInterface;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class ContentEntityNormalizer extends SerializationContentEntityNormalizer {

  /**
   * The normalizer used to normalize the typed data.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\node\NodeInterface';

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeRepositoryInterface $entity_type_repository, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($entity_type_manager, $entity_type_repository, $entity_field_manager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    // If we aren't dealing with an object or the format is not supported return
    // now.
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof NodeInterface) {
      return TRUE;
    }
    // Otherwise, this normalizer does not support the $data object.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    $normalized_entity = parent::normalize($entity, $format, $context);

    // Unset the fields we don't need.
    unset(
      $normalized_entity['uuid'],
      $normalized_entity['type'],
      $normalized_entity['revision_timestamp'],
      $normalized_entity['revision_uid'],
      $normalized_entity['revision_log'],
      $normalized_entity['status'],
      $normalized_entity['uid'],
      $normalized_entity['promote'],
      $normalized_entity['sticky'],
      $normalized_entity['default_langcode'],
      $normalized_entity['revision_translation_affected'],
      $normalized_entity['metatag'],
      $normalized_entity['path'],
      $normalized_entity['menu_link'],
      $normalized_entity['vid']
    );

    return $normalized_entity;
  }

}
