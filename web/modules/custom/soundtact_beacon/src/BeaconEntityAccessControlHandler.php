<?php

namespace Drupal\soundtact_beacon;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Beacon entity.
 *
 * @see \Drupal\soundtact_beacon\Entity\BeaconEntity.
 */
class BeaconEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\soundtact_beacon\Entity\BeaconEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished beacon entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published beacon entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit beacon entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete beacon entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add beacon entities');
  }

}
