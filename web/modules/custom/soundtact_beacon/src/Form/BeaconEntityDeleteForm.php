<?php

namespace Drupal\soundtact_beacon\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Beacon entities.
 *
 * @ingroup soundtact_beacon
 */
class BeaconEntityDeleteForm extends ContentEntityDeleteForm {


}
