<?php

namespace Drupal\soundtact_route\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\soundtact_api\Api\AccessDeniedJsonException;
use Drupal\soundtact_api\Api\ForbiddenJsonException;
use Drupal\soundtact_api\Api\NotFoundJsonException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;

/**
 * Logic for the Route api.
 */
class RouteController extends ControllerBase {

  /**
   * Symfony\Component\Serializer\Serializer definition.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The request from the user.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * RouteController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $currentRequest
   *   The current request.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer to normalize classes.
   */
  public function __construct(Request $currentRequest, Serializer $serializer) {
    $this->currentRequest = $currentRequest;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('serializer')
    );
  }

  /**
   * Get a list of all the routes.
   */
  public function getList() {
    $user_storage = $this->entityTypeManager()->getStorage('user');
    $current_user = $user_storage->load($this->currentUser()->id());
    if ($current_user->hasPermission('access content')) {
      return $this->getListByTheme();
    }

    if ($current_user->hasPermission('access route api')) {
      return $this->getListByUserId();
    }

    return new AccessDeniedJsonException($this->t('You have no access to view the routes.'));
  }

  /**
   * This functions retrieves all the routes by the currents user theme.
   *
   * This function is used for users who have access to all content.
   */
  public function getListByTheme() {
    $user_storage = $this->entityTypeManager()->getStorage('user');

    $current_user = $user_storage->load($this->currentUser->id());
    $user_theme = $current_user->field_theme ? $current_user->field_theme->getValue() : NULL;

    $themes = [];
    foreach ($user_theme as $theme) {
      $themes[] = $theme['target_id'];
    }

    // Get the theme query parameter to base the query on the route theme.
    $entity_storage = $this->entityTypeManager()->getStorage('node');
    $query = $entity_storage->getQuery()
      ->condition('type', 'route');

    if (!empty($themes)) {
      $query->condition('field_theme', $themes, 'IN');
    }

    // Get all route entities.
    $nids = $query->execute();

    // Load all the entities.
    $nodes = $entity_storage->loadMultiple($nids);
    $return_array = [];

    // Because we want the json response smaller then the normalizer returns,
    // we normalize here.
    foreach ($nodes as $key => $node) {
      $title = $node->title ? $node->title->getValue()[0]['value'] : NULL;
      $clients = [];

      foreach ($node->field_clients as $client) {
        $client_id = $client->getValue()['target_id'];
        $client_entity = $user_storage->load($client_id);
        $clients[] = $this->serializer->normalize($client_entity);
      }

      $return_array[] = [
        'nid' => $key,
        'title' => $title,
        'clients' => $clients,
      ];
    }

    $response = new JsonResponse(['routes' => $return_array], 200);
    return $response;
  }

  /**
   * This function retrieves the route by the current users id.
   *
   * This function is used for users who don't have access to all the routes.
   */
  public function getListByUserId() {
    $user_id = $this->currentUser->id();
    $user_storage = $this->entityTypeManager()->getStorage('user');

    // Get the theme query parameter to base the query on the route theme.
    $entity_storage = $this->entityTypeManager()->getStorage('node');
    $query = $entity_storage->getQuery()
      ->condition('type', 'route')
      ->condition('field_clients', [$user_id], 'IN');

    // Get all route entities.
    $nids = $query->execute();

    // Load all the entities.
    $nodes = $entity_storage->loadMultiple($nids);
    $return_array = [];

    // Because we want the json response smaller then the normalizer returns,
    // we normalize here.
    foreach ($nodes as $key => $node) {
      $title = $node->title ? $node->title->getValue()[0]['value'] : NULL;
      $clients = [];

      foreach ($node->field_clients as $client) {
        $client_id = $client->getValue()['target_id'];
        $client_entity = $user_storage->load($client_id);
        $clients[] = $this->serializer->normalize($client_entity);
      }

      $return_array[] = [
        'nid' => $key,
        'title' => $title,
        'clients' => $clients,
      ];
    }

    $response = new JsonResponse(['routes' => $return_array], 200);
    return $response;
  }

  /**
   * Get the detail of a route.
   *
   * The detail includes the sounds to play, notification sound
   * and vibration.
   *
   * @param \Drupal\node\NodeInterface $route
   *   The route from the url.
   */
  public function getDetail(NodeInterface $route) {
    if (!$this->currentUser()->hasPermission('access content')) {
      return new ForbiddenJsonException($this->t('You have no access to get the detail of this route.'));
    }

    if ($route === NULL) {
      return new NotFoundJsonException($this->t('Please specify the route.'));
    }

    $normalized_route = $this->serializer->normalize($route);

    $response = new JsonResponse($normalized_route, 200);
    return $response;
  }

}
