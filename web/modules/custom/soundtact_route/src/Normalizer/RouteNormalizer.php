<?php

namespace Drupal\soundtact_route\Normalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\soundtact_api\Normalizer\ContentEntityNormalizer;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class RouteNormalizer extends ContentEntityNormalizer {

  /**
   * The normalizer used to normalize the typed data.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    if (!parent::supportsNormalization($data, $format)) {
      return FALSE;
    }

    if ($data->getType() == 'route') {
      return TRUE;
    }
    // Otherwise, this normalizer does not support the $data object.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Can't inject serializer due to the following error:
    // [error]  Circular reference detected for service "serializer",
    // path: "serializer -> soundtact_api.typed_data -> serializer".
    // @todo Fix this error in core?
    // @codingStandardsIgnoreStart
    /* @var $object \Drupal\Core\TypedData\TypedDataInterface */
    if (!$this->serializer) {
      $this->serializer = \Drupal::service('serializer');
    }
    // @codingStandardsIgnoreEnd

    $normalized_route = parent::normalize($entity, $format, $context);

    $this->normalizeTerms($entity, $normalized_route);
    $this->normalizePoints($entity, $normalized_route);

    return $normalized_route;
  }

  /**
   * Normalize the term fields of route.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The route.
   * @param array $normalized_route
   *   The array to add the normalized terms to.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function normalizeTerms(ContentEntityInterface $entity, array &$normalized_route) {
    $taxonomy_term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    $notification_id = $entity->field_notification ? $entity->field_notification->getValue()[0]['target_id'] : NULL;
    $vibration_id = $entity->field_vibration ? $entity->field_vibration->getValue()[0]['target_id'] : NULL;

    $notification = $taxonomy_term_storage->load($notification_id);
    if ($notification != NULL) {
      $normalized_route['field_notification'] = $this->serializer->normalize($notification);
    }

    $vibration = $taxonomy_term_storage->load($vibration_id);
    if ($vibration != NULL) {
      $normalized_route['field_vibration'] = $this->serializer->normalize($vibration);
    }
  }

  /**
   * Normalize the points.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The route.
   * @param array $normalized_route
   *   The array to add the normalized terms to.
   */
  private function normalizePoints(ContentEntityInterface $entity, array &$normalized_route) {
    $normalized_route['field_points'] = [];
    foreach ($entity->field_points as $point) {
      $point_storage = $this->entityTypeManager->getStorage('point');
      $point_entity = $point_storage->load($point->getValue()['target_id']);
      $normalized_route['field_points'][] = $this->serializer->normalize($point_entity);
    }
  }

}
