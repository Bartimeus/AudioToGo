/**
 * @file
 * Entity_browser.view.js.
 *
 * Defines the behavior of the entity browser's view widget.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  var attached = false;
  var view_info = {
    view_name: 'media_dialog',
    view_display_id: 'page_1',
  };

  /**
   * Registers behaviours related to the view.
   */
  Drupal.behaviors.soundtactRouteView = {
    attach: function (context) {
      if (attached === false && $('.view-media-dialog').length > 0) {
        // Because the media view doesn't work with ajax we need to reinitialize the
        // ajax view again.
        attached = true;
        initMediaView()
      }
    },
    detach: function (context) {
      attached = false;
    }
  };

  function refreshView(view_wrapper) {
    // Details of the ajax action.
    var ajax_settings = {
      submit: view_info,
      url: '/views/ajax',
    };

    var ajax_object = Drupal.ajax(ajax_settings);
    ajax_object.commands.insert = function (ajax, response, status) {
      view_wrapper.html(response.data);
      initMediaView();
    };
    ajax_object.execute();
  }

  function initMediaView() {
    var media_view = $('.view-media-dialog');
    var media_view_wrapper = $('.media-view-wrapper');

    if (media_view && media_view.first()) {
      var view = media_view.first();
      // Because ajax was not initialized when we load the view
      // the listeners are getting set in here.
      // Set listener on submit button.
      view.find('input[type="submit"]').off().on('click', function (event) {
        event.preventDefault();
        refreshView(media_view_wrapper);
      });

      view.find('.views-table tr').each(function (key, tr) {
        $(tr).find('.add-music-button').on('click', function (event) {
          event.preventDefault();

          // Need to change a hidden input value to the media id
          // so we know which media to add to the point.
          // This media_id is set in the media view.
          var hidden_input = $('.selected-music-input');
          var media_id = $(tr).find('.media-id').data('media-id');
          hidden_input.val(media_id);

          console.info('media_id', media_id);

          $('.submit-existing-music-button').trigger('mousedown');
        });
      });

      view.find('input[type="text"]').each(function () {
        // We have 2 listeners because we need to prevent default on keypress
        // and keypress doesn't register backspace.
        $(this).on('keypress', function (event) {
          // If the key is enter submit.
          if (event.keyCode == 13) {
            event.preventDefault();

            refreshView(media_view_wrapper)
          }
        });

        $(this).on('keyup', function (event) {
            view_info[$(this).attr('name')] = $(this).val();
        });
      });
    }
  }

}(jQuery, Drupal, drupalSettings));
