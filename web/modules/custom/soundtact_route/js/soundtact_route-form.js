/**
 * @file
 */

(function ($, Drupal) {
  var timeout;

  // Hide status messages after a few seconds.
  Drupal.behaviors.soundtactMessages = {
    attach: function () {
      $(document).ajaxComplete(function (event, xhr, settings) {
        clearTimeout(timeout);
        var messages = $(".form-route .messages");
        if (messages && messages.length > 0) {
          timeout = setTimeout(function () {
            $.each(messages, function (key, message) {
              message.parentNode.removeChild(message);
            });
          }, 3000);
        }
      });
    }
  };

  // Variable to check if audio already is set.
  var attachedMusic = false;

  // The audioFile to play.
  var audio;

  // The audioUrl to play.
  var audioUrl;

  /**
   * This behavior is used to play the music when you click on a music title in the route form.
   */
  Drupal.behaviors.soundtactPlayMusic = {
    attach: function () {
      if (!attachedMusic) {
        attachedMusic = true;
        $(".music-title").on("click", musicClickListener);
      }
    },

    detach: function () {
      attachedMusic = false;
      $(".music-title").off("click", musicClickListener);
    }
  };

  function musicClickListener(e) {
    var target = $($(e.target).get(0));
    if (target) {
      var newAudioUrl = target.data("audio-url");

      // First check if audio isn't already playing.
      if (audio !== undefined && audioUrl !== undefined) {
        if (!audio.paused) {
          // Audio is playing.
          // Stop the audio file and if the audio file is different then the last one
          // start that one.
          audio.pause();
          if (newAudioUrl !== audioUrl) {
            audioUrl = newAudioUrl;
            audio = new Audio(newAudioUrl);
            audio.play();
          }
        }
        else {
          audio = new Audio(newAudioUrl);
          audio.play();
        }
      }
      else {
        audio = new Audio(newAudioUrl);
        audioUrl = newAudioUrl;
        audio.play();
      }
    }
  }
})(jQuery, Drupal);
