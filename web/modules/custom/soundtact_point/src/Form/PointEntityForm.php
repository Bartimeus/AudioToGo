<?php

namespace Drupal\soundtact_point\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Point edit forms.
 *
 * @ingroup soundtact_point
 */
class PointEntityForm extends ContentEntityForm {

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, MessengerInterface $messenger, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\soundtact_point\Entity\PointEntity $entity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    $audio_file = $entity->get('audio');
    $audio_file_id = NULL;
    if ($audio_file !== NULL) {
      $audio_file_id = [$audio_file->getValue()[0]['target_id']];
    }

    $form['audio'] = [
      '#type'          => 'managed_file',
      '#title'         => $this->t('Choose Audio File'),
      '#upload_location' => 'public://audio/',
      '#default_value' => $audio_file_id,
      '#description'   => $this->t('Add a Adio file.'),
      '#upload_validators' => [
        'file_validate_extensions' => 'mp3 wav',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger->addMessage($this->t('Created the %label Point.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger->addMessage($this->t('Saved the %label Point.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.point.canonical', ['point' => $entity->id()]);
  }

}
