<?php

namespace Drupal\soundtact_notification\Normalizer;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\soundtact_api\Normalizer\TermNormalizer;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Normalizes all notification terms.
 */
class NotificationNormalizer extends TermNormalizer {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The filesystem.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs an EntityNormalizer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository
   *   The entity type repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeRepositoryInterface $entity_type_repository, EntityFieldManagerInterface $entity_field_manager, FileSystemInterface $file_system) {
    parent::__construct($entity_type_manager, $entity_type_repository, $entity_field_manager);

    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    // If we aren't dealing with an object or the format is not supported return
    // now.
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof TermInterface && $data->bundle() === 'notification') {
      return TRUE;
    }

    // Otherwise, this normalizer does not support the $data object.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    $normalized_entity = parent::normalize($entity);

    $audio_field = $entity->get('field_audio');

    if ($value = $audio_field->getValue()) {
      $file = $this->entityTypeManager->getStorage('file')->load($value[0]['target_id']);
      $normalized_entity['field_audio']['name_extension'] = $this->fileSystem->basename($file->getFileUri());
    }

    return $normalized_entity;
  }

}
