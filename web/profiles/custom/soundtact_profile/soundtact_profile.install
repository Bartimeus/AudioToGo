<?php

/**
 * @file
 * Install, update and uninstall functions for the pvm install profile.
 */

use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Site\Settings;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function soundtact_profile_install() {
  // Set front page to "node".
  \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/node')->save(TRUE);

  // Allow visitor account creation with administrative approval.
  $user_settings = \Drupal::configFactory()->getEditable('user.settings');
  $user_settings->set('register', UserInterface::REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)->save(TRUE);

  // Enable default permissions for system roles.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access comments']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, [
    'access comments',
    'post comments',
    'skip comment approval',
  ]);

  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // We install some menu links, so we have to rebuild the router, to ensure the
  // menu links are valid.
  \Drupal::service('router.builder')->rebuildIfNeeded();

  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['access site-wide contact form']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access site-wide contact form']);

  // Allow authenticated users to use shortcuts.
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['access shortcuts']);

  // Allow all users to use search.
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['search content']);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, ['search content']);

  // Enable the admin theme.
  \Drupal::configFactory()->getEditable('node.settings')->set('use_admin_theme', TRUE)->save(TRUE);

  // Default installation for pvm.
  install_configuration();
}

/**
 * Install all the config from the default config folder.
 */
function install_configuration() {
  preload_config_ignore();

  $log_channel = 'soundtact_profile';

  $source_storage = \Drupal::service('config.storage.sync');
  $active_storage = \Drupal::service('config.storage');

  $system_site = $source_storage->read('system.site');
  \Drupal::configFactory()->getEditable('system.site')->set('uuid', $system_site['uuid'])->save();

  /** @var \Drupal\Core\Config\ConfigManagerInterface $config_manager */
  $config_manager = \Drupal::service('config.manager');
  $storage_comparer = new StorageComparer($source_storage, $active_storage, $config_manager);

  $config_importer = new ConfigImporter(
    $storage_comparer,
    \Drupal::service('event_dispatcher'),
    \Drupal::service('config.manager'),
    \Drupal::lock(),
    \Drupal::service('config.typed'),
    \Drupal::moduleHandler(),
    \Drupal::service('module_installer'),
    \Drupal::service('theme_handler'),
    \Drupal::service('string_translation')
  );

  if (!$storage_comparer->createChangelist()->hasChanges()) {
    \Drupal::logger($log_channel)->info(('There are no changes to import.'));
  }

  try {
    // This is the contents of \Drupal\Core\Config\ConfigImporter::import.
    // Copied here so we can log progress.
    if ($config_importer->hasUnprocessedConfigurationChanges()) {
      \Drupal::logger($log_channel)->info('Starting import');
      $sync_steps = $config_importer->initialize();
      foreach ($sync_steps as $step) {
        $context = [];
        do {
          $config_importer->doSyncStep($step, $context);
          if (isset($context['message'])) {
            \Drupal::logger($log_channel)->info(str_replace('Synchronizing', 'Synchronized', (string) $context['message']));
          }
        } while ($context['finished'] < 1);
      }
    }
    if ($config_importer->getErrors()) {
      \Drupal::logger($log_channel)->error('Errors occurred during import');
    }
    else {
      \Drupal::logger($log_channel)->info('The configuration was imported successfully.');
    }
  }
  catch (ConfigException $e) {
    \Drupal::logger($log_channel)->error($e->getMessage());
  }
}

/**
 * Enable the config.
 */
function preload_config_ignore() {
  global $config_directories;

  \Drupal::service('module_installer')->install([
    'config_split',
    'config_ignore',
  ]);

  // Pre-load config_split configucation
  // or any path containing config files theoretically.
  $config_path = $config_directories[Settings::get('config_sync_directory')];
  $source = new FileStorage($config_path);
  $config_storage = \Drupal::service('config.storage');
  $config_storage->write('config_split.config_split.development', $source->read('config_split.config_split.development'));
  $config_storage->write('config_ignore.settings', $source->read('config_ignore.settings'));
}
